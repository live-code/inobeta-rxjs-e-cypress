// cypress/e2e/login/login-page.cy.ts
import LoginPage from './login.page';

describe("Login", () => {
  beforeEach(() => {
    LoginPage.visit()
  })

  it('the SignIn button should be disabled by default', () => {
    LoginPage.isDisabled();
  })

  it('the SignIn button should be disabled if validation fails', () => {
    LoginPage.fill('ma', '12')
    LoginPage.isDisabled();
  })

  it('the SignIn button should be enabled if validation is successful', () => {
    LoginPage.fill('mario', '12345')
    LoginPage.isButtonEnabled();
  })

  it('visit home after login', () => {
    LoginPage.signIn('mario', '12345')
  })
});
