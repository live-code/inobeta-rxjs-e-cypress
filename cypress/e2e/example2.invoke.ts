describe('Cypress / Angular playground', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200')
  })

  it('passed!', () => {
   cy.get('label')
      .contains('first name', { matchCase: false})
      .invoke('prop', 'for')
      .then(val => {
        cy.get('#' + val).type('pippo')
      })
  })

})


/*
// app.component
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule],
  template: `
      <div>
          <div>
              <label [for]="dynamic">First Name</label>
          </div>

          <div>
              <input type="text" [id]="dynamic" />
          </div>
      </div>
  `,
})
export class AppComponent {
  dynamic = 'item'+Date.now()
}

 */
