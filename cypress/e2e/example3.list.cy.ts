// cypress/e2e/playground.cy.ts
import { AppComponent } from '../../src/app/app.component';
import { ITEMS } from '../../src/app/data';

describe('Home page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/');
  })

  it("first element is 'one'", () => {

    cy.get('main' )
      .find('article')
      .each(($el, index) => {
        console.log($el)
        cy.wrap($el)
          .should('have.class', 'item' + index)

        cy.wrap($el)
          .contains(ITEMS[index])
      })
  })



})


/*
// app.component
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ITEMS } from './data';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule],
  template: `
      <main>
          <article
            *ngFor="let item of items; let i = index"
            [class]="'item'+i"
          >
              {{item}}
          </article>
      </main>
  `,
})
export class AppComponent {
  items = ITEMS;
}

 */
