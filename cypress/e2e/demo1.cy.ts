import { User } from '../../src/model/user';

const mockList: User[] = [
  { id: 1, name: "A", username: "Username A" },
  { id: 2, name: "B", username: "Username B" },
  { id: 3, name: "C", username: "Username C" },
  { id: 4, name: "D", username: "Username D" },
]

describe("Hello Cypress", () => {

  beforeEach(() => {

    cy.intercept(
      'https://jsonplaceholder.typicode.com/users',
      { method: 'GET'},
      mockList
    )

    cy.visit('http://localhost:4200/demo1')
    cy.get('.list').as('list')
  })

  it(`should display exactly ${mockList.length} elements in the page`, () => {
    cy.get('@list').children().should('have.length', mockList.length)
  });

  it(`should the first and last item display its own name and username`, () => {
    const firstItem = mockList[0];
    cy.get('@list').children().first().contains(firstItem.name)
    cy.get('@list').children().first().contains(firstItem.username)

    const lastItem = mockList[mockList.length - 1];
    cy.get('@list').children().last().contains(lastItem.name)
    cy.get('@list').children().last().contains(lastItem.username)
  });

  it("list contains all items", () => {
    mockList.forEach((item) => {
      cy.contains(item.name)
      cy.contains(item.username)
    })
  })
  // alternative to previous way
  it(`should all items display its own name and username`, () => {
    cy.get('@list')
      .children()
      .each(($el, index) => {
        cy.wrap($el).contains(mockList[index].name)
        cy.wrap($el).contains(mockList[index].username)
      })
  });

  it(`should delete an item`, () => {
    cy.get("@list").children().first().contains('Del').click()
    cy.get("@list").children().should('have.length', mockList.length - 1)
  });


  it(`should add an item`, () => {
    cy.get('input').type('Fabio')
    cy.get('button').contains('add').click()
    cy.get("@list").children().should('have.length', mockList.length + 1)
  });

});


// NEW
describe.only("Error Handling", () => {

  it(`should display an error message if API GET fails`, () => {
    cy.intercept(
      'https://jsonplaceholder.typicode.com/users',
      { method: 'GET' },
      {
        statusCode: 404,
        body: 'some errors here'
      }
    )
    cy.visit('http://localhost:4200')
    cy.contains('Server error')
  });


  it(`should not delete an item if API DELETE fails`, () => {
    cy.intercept(
      'https://jsonplaceholder.typicode.com/users',
      { method: 'GET' },
      mockList
    )

    cy.intercept(
      'https://jsonplaceholder.typicode.com/users/**',
      { method: 'DELETE' },
      {
        statusCode: 404,
        body: 'some errors here'
      }
    )

    cy.visit('http://localhost:4200')
    cy.get('.list').as('list')
    cy.get('@list').as("userList");
    cy.get('@list').children().first().contains('Del').click()
    cy.get('@list').children().should('have.length', mockList.length)
    cy.contains('Server error')
  });


  it(`should not add an item if API POST fails`, () => {

    cy.intercept(
      'https://jsonplaceholder.typicode.com/users',
      { method: 'GET' },
      mockList
    )

    cy.intercept(
      'https://jsonplaceholder.typicode.com/users/',
      { method: 'POST' },
      {
        statusCode: 404,
        body: 'some errors here'
      }
    )

    cy.visit('http://localhost:4200')
    cy.get('.list').as('list')
    cy.get('input').type('Fabio')
    cy.get('button').contains('add').click()
    cy.get("@list").children().should('have.length', mockList.length)
  });
});
