class LoginPage {
  elements = {
    username: () => cy.get(`input[placeholder="Username"]`),
    password: () => cy.get(`input[placeholder="Password"]`),
    button: () => cy.contains('SIGN IN')
  }

  visit() {
    cy.visit('http://localhost:4200/login')
  }

  fill(u: string, p: string) {
    this.elements.username().type(u);
    this.elements.password().type(p);
  }

  signIn(u: string, p: string) {
    // cy.login()
    this.fill(u, p)
    this.elements.button().click()
    cy.url().should('include', 'demo1')
  }

  isDisabled() {
    this.elements.button().should('be.disabled');
  }
  isButtonEnabled() {
    this.elements.button().should('be.enabled');
  }
}

export default new LoginPage();
