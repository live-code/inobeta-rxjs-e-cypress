describe('template spec', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/')
    cy.get('form.second').as('formX')
  })

  it('passes 1', () => {
    // dialog
    cy.get('@formX')
      .within(() => {
        cy.get('label')
          .contains('Name')
          .should('be.visible')
          .and('have.attr', 'title', 'pippo')
          .and('have.attr', 'title', 'pippo')

        cy.get('input')
          .type('ciccio')

      })
  })

  it('passes 2', () => {

    // dialog
    cy.get('@formX')
      .within(() => {
        cy.get('label')
          .contains('Name')
          .should('be.visible')
          .and('have.attr', 'title', 'pippo')
          .and('have.attr', 'title', 'pippo')


        cy.get('input')
          .type('ciccio')

      })

  })



})


/*
  <form class="first">
          <div>
              <label>Name</label>
              <input type="text"/>
          </div>
      </form>

      <form class="second">
          <div>
              <label title="pippo">Name</label>
              <input type="text"/>
          </div>
      </form>
 */
