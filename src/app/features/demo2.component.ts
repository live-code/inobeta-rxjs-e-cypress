import { Component } from '@angular/core';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [],
  template: `
    <p>
      demo2 works!
    </p>
  `,
  styles: ``
})
export default class Demo2Component {

}
