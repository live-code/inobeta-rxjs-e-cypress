// app.component
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../../model/user';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule],
  template: `
      <div *ngIf="error">Server error</div>

      <input type="text" #inputRef>
      <button (click)="add(inputRef.value)">add</button>

      <ul class="list">
          <li *ngFor="let u of users">
              {{u.name}} ({{u.username}})
              <button (click)="delete(u.id)">Del</button>
          </li>
      </ul>
  `,
})
export default class Demo1Component  implements OnInit {
  http = inject(HttpClient)
  users: User[] = [];
  error: boolean = false;

  ngOnInit(): void {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe({
        next: res => this.users = res,
        error: () => this.error = true
      })
  }

  delete(id: number): void {
    this.error = false;
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .subscribe({
        next: () => {
          this.users = this.users.filter(u => u.id !== id)
        },
        error: () => this.error = true
      })
  }

  add(name: string) {
    this.error = false;
    this.http.post<User>('https://jsonplaceholder.typicode.com/users', {
      name
    })
      .subscribe({
        next: res => {
          this.users = [...this.users, res]
        },
        error: () => this.error = true
      })
  }


}
