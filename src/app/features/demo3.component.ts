import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { BehaviorSubject, delay, interval, share, shareReplay, Subject, take } from 'rxjs';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    AsyncPipe,
    JsonPipe
  ],
  template: `
      <p>
          demo3 works!
      </p>

  `,
  styles: ``
})
export default class Demo3Component {
  http = inject(HttpClient)
  timer$ = interval(1000)
  timer$Subject = new BehaviorSubject<number | null>(null);

  constructor() {

    this.timer$.subscribe(this.timer$Subject)

    this.timer$Subject.subscribe(val => console.log('A', val))

    setTimeout(() => {
      this.timer$Subject.subscribe(val => console.log('B', val))
    }, 2000)


    const subject  = new BehaviorSubject(0)
    subject.next(10);
    subject.next(20);
    console.log(subject.getValue())
    subject.subscribe({
      next: (val) => console.log('val' , val),
      error: (err) => console.log('err', err),
    })
    subject.next(30);
    subject.next(30);
    subject.next(30);
    subject.next(30);

  }

}
