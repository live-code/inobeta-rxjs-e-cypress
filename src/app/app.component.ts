import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, RouterLink],
  template: `
      <button routerLink="login">login</button>
      <button routerLink="demo1">demo1</button>
      <button routerLink="demo2">demo2</button>
      <button routerLink="demo3">demo3</button>

      <hr>
      <router-outlet></router-outlet>
  `,
  styles: [],
})
export class AppComponent {
  title = 'inobeta-2';
}
